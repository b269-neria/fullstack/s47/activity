// "document" refers to the whole webpage
// "querySelector" is used to select a specific object (HTML element) from our document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#txt-last-name");


document.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
});